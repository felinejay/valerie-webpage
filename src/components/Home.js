import React, { useState } from 'react';
import { Grid, Paper, TextField, Button } from '@mui/material';
import CommentContainer from './CommentContainer';

const Home = () => {

	const [comment, setComment] = useState('');
	const [author, setAuthor] = useState('');

	const paperStyle = {padding: 20, height: '85vh', width: 720, margin: '20px auto', overflowY: 'scroll'};

	const saveComment = async (e) => {
		e.preventDefault();
		if (comment !== '' && author !== '') {
			const result = await fetch('http://localhost:3004/comments', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					comment,
					author
				})
			});
			if(result.status === 201) {
				setComment('');
				setAuthor('');

			} else {
				alert('Invalid comment');
			}
		} else {
			alert('Invalid comment: Required fields are empty!');
		}		
	};

	const handleCommentInput = (e) => {
		setComment(e.target.value);
	};
	const handleAuthorInput = (e) => {
		setAuthor(e.target.value);
	};

	return (
		<div className="formContainer">
			<Grid>
				<Paper elevation={10} style={paperStyle}>
					<Grid align="center">
						<div className="form-inner">
							<h2>Comment section</h2>
						</div>
					</Grid>
					<form>
						<TextField variant="standard" id="commentAuthor" value={author} onChange={handleAuthorInput} label="Name" placeholder="Enter your name" required/>
						<TextField variant="standard" id="comment" value={comment} onChange={handleCommentInput} label="Comment" placeholder="Enter your comment" fullWidth required/>

						<Button type="submit" variant="contained" color="success" style={{marginTop: 10, marginRight: 20}} onClick={saveComment} >Send comment</Button>
					</form>	
					<CommentContainer />
				</Paper>
			</Grid>

		</div>
	);
};

export default Home;
