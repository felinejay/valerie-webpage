import React from 'react';
import commandsPNG from '../images/Commands.png';
import commands2PNG from '../images/Commands2.png';

const Documentation = () => {
	return (
		<main>
			<section className="py-5 text-center container">
				<div className="row py-lg-5">
					<div className="col-lg-6 col-md-8 mx-auto">
						<h1 className="fw-light text-white bg-transparent rounded">Bot documentation</h1>
						<p className="lead text-white bg-transparent rounded">Valerie the bot started in 2019 as a Python bot originally but eventually I went for a JavaScript approach with her. Check out what she can do and test her yourself on the Demo page!</p>
					</div>
				</div>
			</section>
			<div className="album py-5 bg-transparent">
				<div className="container">
					<div className="col">
						<div className="card shadow-sm" >
							<img src={commandsPNG} />
							<div className="card-body">
								<p className="card-text">Valerie knows many commands(two of them being .help and .cute), some of the commands are not for the public use and some commands are only known by Jay. Feel free to test them out on the Demo page!</p>
							</div>
						</div>
						<div className="card shadow-sm" style={{marginTop: 20}}>
							<img src={commands2PNG} />
							<div className="card-body">
								<p className="card-text">Here are most of the commands and some code example of one of the commands that uses database!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	);
};

export default Documentation;
