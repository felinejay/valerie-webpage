import './App.css';
import React from 'react';
import Demo from './components/Demo';
import Documentation from './components/Documentation';
import Creator from './components/Creator';
import Home from './components/Home';
import NavBar from './components/NavBar';
import { Route, Routes } from 'react-router-dom';


function App() {
	return (
		<div>
			<NavBar />
			<Routes>
				<Route path="/" exact element={<Home />} />
				<Route path="/creator" element={<Creator />} />
				<Route path="/documentation" element={<Documentation />} />
				<Route path="/demo" element={<Demo />} />
			</Routes>
		</div>
	);
}

export default App;
